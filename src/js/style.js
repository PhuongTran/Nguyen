
/* ===== WOW ===== */
new WOW().init();


$(document).ready(function(){
	
	$('.box-show-menu').click(function(){
		$('.menu-left').addClass('is-open');
		$('.content-overlay').addClass('is-open');
	});
	$('.content-overlay').click(function(){
		$('.menu-left').removeClass('is-open');
		$(this).removeClass('is-open');
	});
	
	
	
	var classInfo = $('.list-info-profile'),
		menuLeft = $('.menu-left-container');
	
	if(classInfo.length && menuLeft.length){
		var listInfo = classInfo.position().top,
			menuLeftWidth = menuLeft.width();
	}
	
	scrollWindow();
	
	$(window).resize(function() {
		menuLeftWidth = menuLeft.width();
		scrollWindow();
	});
	
	function scrollWindow(){
		$('.list-info-profile').css('width',menuLeftWidth)
		$(window).scroll(function() {
			var scrollTop = $(this).scrollTop();
				
			if(scrollTop > 0){
				$('.header-employees').addClass('header-employees-border');
			}else{
				$('.header-employees').removeClass('header-employees-border');
			}
			
			if(scrollTop > listInfo){
				$('.list-info-profile').css({
					'position':'fixed',
					'width': menuLeftWidth,
					'left':'290px',
					'z-index':'9',
					'top':listInfo - 100
				});
			}else{
				$('.list-info-profile').css({
					'position':'static'
				});
			}
		});	
	}
	
	var hieghtHeader = $('.box-flex-search').height();

	$(document).on("scroll", onScroll);

    $('a.scrollLink').on('click', function (e) {
        e.preventDefault();
        $('a.scrollLink').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var target = this.hash,
              menu = target;
        $target = $(target);
		
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top - (hieghtHeader + 20)
        });
    });
	
	
	/* ===== Tooltip ===== */
	$('[data-toggle="tooltip"]').tooltip(); 
	
	
	$(".icon-close-messages").click(function(){
		$(this).parent().hide();	
	});
	
	
	$().fancybox({
		selector : '[data-fancybox="filter"]:visible',
		thumbs   : {
			autoStart : true
		}
	});

	
	$('.modal').on("hidden.bs.modal", function (e) {
        if ($('.modal:visible').length) {
            $('body').addClass('modal-open');
        }
    });
	
}); 

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.list-info-profile a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.list-info-profile a').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}
